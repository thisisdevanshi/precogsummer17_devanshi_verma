#Script to Collect Data from Twitter using Streaming API

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from pymongo import MongoClient
import json


access_token = ' '
access_token_secret = ' '
consumer_key = ' '
consumer_secret = ' '



class CreateListener(StreamListener):

    def on_data(self, data):
        client = MongoClient('localhost', 27017)
        db = client['twitter_db']
        collection = db['twitter_collection']
        tweet = json.loads(data)
        collection.insert(tweet)
        return True

    def on_error(self, status):
        print (status)


if __name__ == '__main__':

    l = CreateListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, l)

    stream.filter(track=['#USelection'])


