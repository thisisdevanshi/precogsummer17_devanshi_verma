#Geographical Distribution of tweets
from motionless import DecoratedMap, AddressMarker
import pandas as pd

def plot(i):
    try:
        if(not(pd.isnull(i))):
            dmap.add_marker(AddressMarker(str(i),color='yellow',size='small'))
        
    except Exception, e:
        pass

data=pd.read_csv('example.csv',header=None,encoding="ISO-8859-1")


df=pd.DataFrame(data.values)
df.columns=['sno','location','tweet']

road_styles = [{
    'feature': 'road.highway',
    'element': 'geomoetry',
    'rules': {
        'visibility': 'simplified',
        'color': '#c280e9'
    }
}, {
    'feature': 'transit.line',
    'rules': {
        'visibility': 'simplified',
        'color': '#bababa'
    }
}]

dmap = DecoratedMap(style=road_styles,size_x=640, size_y=440)

#Limitation as per length of URL
for i in df.loc[0:120,'location']:
    plot(i)
    

print (dmap.generate_url())
