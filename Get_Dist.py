#A tweet without url is just text based tweet,whereas a tweet with URL represents image
#Yellow Color= Text with image, Red Color= Text

import re
import pandas as pd
from motionless import DecoratedMap, AddressMarker

def plot(i):
    try:
        if(not(pd.isnull(i))):
            dmap.add_marker(AddressMarker(str(i),color='yellow',size='tiny'))
        
    except Exception, e:
        pass

def plot1(i):
    try:
        if(not(pd.isnull(i))):
            dmap.add_marker(AddressMarker(str(i),color='red',size='tiny'))
        
    except Exception, e:
        pass


road_styles = [{
    'feature': 'road.highway',
    'element': 'geomoetry',
    'rules': {
        'visibility': 'simplified',
        'color': '#c280e9'
    }
}, {
    'feature': 'transit.line',
    'rules': {
        'visibility': 'simplified',
        'color': '#bababa'
    }
}]
dmap = DecoratedMap(style=road_styles,size_x=640, size_y=440)




data=pd.read_csv('example.csv',header=None,encoding="ISO-8859-1")
df=pd.DataFrame(data.values)
df.columns=['sno','location','tweet']


locations=[]
locations1=[]


j=0
l=[]
for i in df.loc[0:100,'tweet']:
    if(re.search(r'https://[\w.]+\S+',i)):
        l.append(j)
    j=j+1

m=0
for i in df.loc[0:100,'location']:
    if(m in l):
        locations.append(i)
    else:
    	locations1.append(i)
    m=m+1


for i in locations:
    plot(i)

for i in locations1:
    plot1(i)


print dmap.generate_url()
