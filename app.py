from flask import Flask, render_template
import flask

# create the application object
app = Flask(__name__)

# use decorators to link the function to a url
@app.route('/')
def home():
	return render_template('home.html')

@app.route('/Top10.html')
def Top10():
	return render_template('Top10.html')


@app.route('/Pop.html')
def Pop():
	return render_template('Pop.html')

@app.route('/GD.html')
def GD():
	return flask.redirect("https://maps.google.com/maps/api/staticmap?maptype=roadmap&format=png&scale=1&size=640x440&sensor=false&language=en&markers=|size:small|color:yellow|Burnley%20England%20|Mumbai%2C%20India|Brussels%2C%20Paris|San%20Antonio%2C%20TX|Ensenada%2C%20Baja%20California|Kansas%20City%2C%20MO|The%20West|PTA%2C%20Gauteng%2C%20S.A|Las%20Vegas%2C%20NV|Mumbai%20and%20around%20your%20city|Ocoee%2C%20FL|United%20States|Massachusetts|Memphis%2C%20TN|New%20Zealand|Prescott%20Valley%2C%20AZ|Missouri%2C%20USA|Hawaii%2C%20USA|Pittsburgh%2C%20PA|New%20Delhi%2C%20India|Germany|Las%20Vegas%2C%20NV|Deer%20Park%2C%20Victoria%2C%20Australia|BEIJING%2C%20CHINA|Indonesia|Perth%2C%20Western%20Australia|Northern%20Rural%20Illinois.|Fl%20native%20|Wellington%2C%20New%20Zealand|New%20Delhi|Islamabad%20PAK|%23TorontoTheGood|Toronto|Kolkata%2C%20India|Sydney%2C%20New%20South%20Wales%20|Australia|%20a%20Philly%20suburb|Kansas%2C%20USA|Toronto%2C%20Ontario%20-%20Canada|%20a%20Philly%20suburb|Manhattan%2C%20NY|Guelph%20Ontario%20Canada%20|%20a%20Philly%20suburb|Londinium|Minneapolis%2C%20MN|Islamabad%20PAK|Australia%20Sydney%20Mate|Jakarta%2C%20Indonesia%20|Cambridge%2C%20Ontario|Washington%20State|Salt%20Lake%20City%2C%20Utah|Las%20Vegas%2C%20NV|California%2C%20USA|UK|Texas%2C%20USA|Sydney%2C%20New%20South%20Wales%20|Alabama%2C%20USA|Dallas%2C%20TX|NJ|Montesano%2C%20WA|setauket%20ny|Surat%2C%20India|Dubai%2C%20UAE|Northern%20California|Arizona%2C%20USA|Redlands|Dearborn%2C%20MI|originally%20from%20Boston|Islamabad%20PAK|Portland%2C%20Oregon|Los%20Angeles%20%2C%20New%20York%2C%20%20Texas|NYC|Carver%2C%20MN|Liberal%20Hell%20California%21%21|God%20Bless%20Texas|Texas%2C%20USA|Islamabad%20PAK|Ladera%20Ranch%2C%20CA%20USA|USA|Canada|United%20States|Connecticut%2C%20USA|The%20World%20Is%20My%20Playground|Noord-Brabant%2C%20Nederland&style=feature:road.highway|element:geomoetry|color:0xc280e9|visibility:simplified|&style=feature:transit.line|element:all|color:0xbababa|visibility:simplified|", code=302)


@app.route('/Dist.html')
def dist():
	return flask.redirect("https://maps.google.com/maps/api/staticmap?maptype=roadmap&format=png&scale=1&size=640x440&sensor=false&language=en&markers=|size:tiny|color:yellow|Burnley%20England%20|Mumbai%2C%20India|Brussels%2C%20Paris|San%20Antonio%2C%20TX|Kansas%20City%2C%20MO|Las%20Vegas%2C%20NV|Mumbai%20and%20around%20your%20city|Ocoee%2C%20FL|United%20States|Massachusetts|Memphis%2C%20TN|New%20Zealand|Prescott%20Valley%2C%20AZ|Missouri%2C%20USA|Hawaii%2C%20USA|Pittsburgh%2C%20PA|Germany|Las%20Vegas%2C%20NV|Deer%20Park%2C%20Victoria%2C%20Australia|BEIJING%2C%20CHINA|Northern%20Rural%20Illinois.|Fl%20native%20|Wellington%2C%20New%20Zealand|Islamabad%20PAK|%23TorontoTheGood|Sydney%2C%20New%20South%20Wales%20|Australia|Kansas%2C%20USA|%20a%20Philly%20suburb|Manhattan%2C%20NY|Guelph%20Ontario%20Canada%20|%20a%20Philly%20suburb|Minneapolis%2C%20MN|Islamabad%20PAK|Australia%20Sydney%20Mate|Jakarta%2C%20Indonesia%20|Washington%20State|Salt%20Lake%20City%2C%20Utah|Las%20Vegas%2C%20NV|California%2C%20USA|UK|Texas%2C%20USA|Sydney%2C%20New%20South%20Wales%20|Alabama%2C%20USA|Dallas%2C%20TX|Montesano%2C%20WA|setauket%20ny|Northern%20California|Arizona%2C%20USA|Redlands|Dearborn%2C%20MI|originally%20from%20Boston|Islamabad%20PAK|Portland%2C%20Oregon&markers=|size:tiny|color:red|Ensenada%2C%20Baja%20California|The%20West|PTA%2C%20Gauteng%2C%20S.A|New%20Delhi%2C%20India|Indonesia|Perth%2C%20Western%20Australia|New%20Delhi|Toronto|Kolkata%2C%20India|%20a%20Philly%20suburb|Toronto%2C%20Ontario%20-%20Canada|Londinium|Cambridge%2C%20Ontario|NJ|Surat%2C%20India|Dubai%2C%20UAE&style=feature:road.highway|element:geomoetry|color:0xc280e9|visibility:simplified|&style=feature:transit.line|element:all|color:0xbababa|visibility:simplified|")

# start the server with the 'run()' method
if __name__ == '__main__':
    app.run(debug=True)