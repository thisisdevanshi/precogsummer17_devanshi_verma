#Get top 10 Hashtags
import re
import pandas as pd
from nltk import FreqDist
import nltk


data=pd.read_csv('example.csv',header=None,encoding="ISO-8859-1")



df=pd.DataFrame(data.values)
df.columns=['sno','location','tweet']

allhashtags=[]

for i in df.loc[0:5310,'tweet']:
	i=str(i)
	allhashtags=allhashtags+re.findall(r'#(\w+)',i)


print(allhashtags)
Freq=FreqDist(allhashtags)

print(Freq.most_common(50))